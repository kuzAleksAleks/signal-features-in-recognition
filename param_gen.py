#!/usr/bin/python -tt

"""A Python program for speech signal feature extraction 
"""

import sys
import os
from struct import unpack, pack
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
import scipy
import scipy.signal
import mlpy.wavelet as wave
from htkwrapper import HTKParamFormat

from abc import ABCMeta, abstractmethod

HTK_TIME_UNITS_NORM = 1e7

class Transformer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def transform(self, sig, leftBord, rightBord):
        pass

    @abstractmethod
    def initTransform(self):
        pass

class FFTTransformer(Transformer):
    def transform(self, sig, leftBord, rightBord, Fs=16000):
        if len(self.frqs) == 0:
            dt = 1.0 / self.Fs
            sigFrameLen = rightBord - leftBord
            T = sigFrameLen * dt
            df = 1.0 / T
  
            self.frqs = np.arange(0, Fs / 2, df)

        S = fft(sig[leftBord:rightBord])
        absS = np.abs(S[:len(S) // 2])

        sigParams = absS
        sigParams = np.float32(sigParams)
        return sigParams

    def initTransform(self, Fs=16000):
        self.Fs = Fs
        self.frqs = []

class WavletTransform(Transformer):
    def __init__(self, wf='morlet', p=6):
        self.wf = wf
        self.omega0 = p
        self.sigImage = []
        self.scales = []
        self.frqs = []

    def initTransform(self, Fs=16000):
        self.Fs = Fs
        self.sigImage = []

    def transform(self, sig, leftBord, rightBord):
        dt = (1.0 / self.Fs)
        if len(self.sigImage) == 0:
            scales = wave.autoscales(N=8100, dt=dt, dj=0.5, 
                         wf=self.wf, p=self.omega0)

            if len(self.scales) == 0:
                print "New scales"
                self.scales = scales
                fperiods = wave.fourier_from_scales(scales=scales, wf=self.wf, p=self.omega0)
                self.frqs = 1 / fperiods
    
            
            self.sigImage = wave.cwt(x=sig, dt=dt, scales=self.scales, 
                             wf=self.wf, p=self.omega0)

        absS = np.absolute(self.sigImage[:, (leftBord + rightBord) // 2])
#        absS = np.abs(np.average(self.sigImage[:, leftBord : rightBord], 1))

        sigParams = absS
        sigParams = np.float32(sigParams)
        
        return sigParams


class Extractor(object):
    def __init__(self, configFile, transClass, *args):
        self.pFormat = HTKParamFormat(configFile)
        self.config = self.pFormat.config
        self.transformer = transClass(*args)

          
    def transform(self, sig):
        smplsInWind = int(self.config['Fs'] * \
            (self.config['WINDOWSIZE'] / HTK_TIME_UNITS_NORM))
        smplsInTimeShift = int(self.config['Fs'] * \
            (self.config['TARGETRATE'] / HTK_TIME_UNITS_NORM))

        self.transformer.initTransform(self.config['Fs'])
        
        transData = []
        nSamples = 0
        for smplInd in range(0, len(sig) - smplsInWind, smplsInTimeShift):
            transData.append(self.transformer.transform(sig, smplInd, 
                                                        smplInd + smplsInWind))
            nSamples += 1

        transData = np.array(transData)
        transData = transData.transpose()
  #      logTransData = np.log(transData)
        
        filtData = []
        windType = 'hamming' 
        fAppl = FilterApplier(windType, len(sig), self.config['Fs']) # len(sig) -- for filters faxis
        fAppl.gen_filt_bank()
        for sampleInd in range(transData.shape[1]):
            
            sigParams = fAppl.applyFilters(transData[:, sampleInd], 
                                           self.transformer.frqs) # frqs -- for sigIm faxis
            
            filtData.append(sigParams)

        filtData = np.array(filtData)
        filtData = filtData.transpose()
        #logFiltData = np.log(filtData)

        paramVect = filtData

        #differ = Differentiator(2)
        #diffs = differ.diff(paramVect)
        #diffsDiffs = differ.diff(diffs)
        
        #paramVect = np.vstack([paramVect, diffs, diffsDiffs])

        transDataStr = ''

        for sampleInd in range(paramVect.shape[1]):
            sample = ''
            for val in paramVect[:, sampleInd]:
                sample += pack('>f', val)
            transDataStr += sample

        #sampSize = len(self.transformer.transform(sig, 0, smplsInWind))
        sampSize = len(sample)
        print sampSize
        parmKind = 9

        transSigStr = self.pFormat.HTK_header(nSamples, self.config['TARGETRATE'], 
                                       sampSize, parmKind) + transDataStr

        return transSigStr

class Differentiator:
    def __init__(self, winLen):
        self.winLen = winLen

    def _single_vect_diff(self, data, ind):
        denom = self.winLen * (self.winLen + 1) * (2 * self.winLen + 1) / 6.0
        denom *= 2
        num = np.zeros(data.shape[0], dtype=data.dtype)
        leftInd, rightInd = ind, ind
        for i in range(1, self.winLen + 1):
            if leftInd - 1 >= 0:
                leftInd -= 1
            if rightInd < data.shape[1] - 1:
                rightInd += 1
            num += i * (data[:, rightInd] - data[:, leftInd])
        return num / denom

    def diff(self, data):
        diffs = np.array([self._single_vect_diff(data, i) \
                            for i in range(data.shape[1])])
        diffs = diffs.transpose()

        return diffs
   
class FilterBank:
    def __init__(self, ftype):
        self.ftype = ftype
        self.filtBank = []

    @staticmethod
    def frq2mel(f):
        return 1127.0 * np.log(1.0 + f / 700.0)

    @staticmethod
    def mel2frq(m):
        return  (np.exp(m / 1127.0) - 1.0) * 700.0

    def gen(self, sigLen, chTotal=0, Fs=16000):
        windKind = scipy.signal.__dict__[self.ftype]
        dt = 1.0 / Fs
        T = sigLen * dt
        df = 1.0 / T
        self.fAxis = np.arange(0, Fs / 2, df)
        
        if (chTotal <= 0):
            chTotal = int(np.ceil(4.6 * np.log10(Fs)))

        mlo = FilterBank.frq2mel(0.0)
        mhi = FilterBank.frq2mel(Fs / 2)
        mb = mhi - mlo

        mc = np.zeros(chTotal)
        for chInd in range(chTotal):
            mc[chInd] = ((chInd + 1) / np.float(chTotal)) * mb + mlo

        fc = map(FilterBank.mel2frq, mc)

        self.filtBank = []
        lInd = 0
        rInd = int(fc[1] / df)
        for chInd in range(2, chTotal + 1):
            N = rInd - lInd
            trw = windKind(N)

            F = np.zeros(len(self.fAxis))
            F[lInd:rInd] = trw

            self.filtBank.append(SigFilter(F, sigLen, Fs))

            if (chInd > 19):
                break

            lInd = int(fc[chInd - 2] / df)
            rInd = int(fc[chInd] / df)

    def __getitem__(self, ind):
        return self.filtBank[ind]

    def __len__(self):
        return len(self.filtBank)

def sig_from_file(filename):
    f = open(filename, 'rb')
    
    block = f.read(4)
    
    smplsTotal = unpack('>I', block)[0]
    class WrongBites(Exception):
        pass
    if smplsTotal > 2**32 - 1:
        raise WrongBites
    
    block = f.read(8)

    sig = np.zeros(smplsTotal)
  
    for smlInd in xrange(smplsTotal):
        block = f.read(2)
        sig[smlInd] = unpack('>h', block)[0]

    f.close()

    return sig

def save_data(data, filename):
    f = open(filename, 'wb')
    f.write(data)
    f.close()

class SigFilter:
    def __init__(self, filt, sigLen, fs=16000):
        self.mapedVals = filt
        self.fs = fs
        self.sigLen = sigLen
    def set_faxis(self, frqs):
        self.frqs = frqs
    def __mul__(self, sigIm):
        dt = 1.0 / self.fs
        T = self.sigLen * dt
        df = 1.0 / T
        filtIm = np.zeros(len(self.frqs), dtype=sigIm.dtype)

        for frqInd in range(len(self.frqs)):
            succInd = int(self.frqs[frqInd] / df) # frequence scale doesn't reach Nyquest frequence
            if succInd >= len(self.mapedVals):
                succInd = len(self.mapedVals) - 1
            filtIm[frqInd] = self.mapedVals[succInd] * np.abs(sigIm[frqInd])
        return filtIm
    def __getitem__(self, ind):
        return self.mapedVals[ind]

    def __len__(self):
        return len(self.mapedVals)

class FilterApplier:
    def __init__(self, filtWinType, fullSigLen, Fs=16000):
        self.fbank = FilterBank(filtWinType)
        self.fullSigLen = fullSigLen
        self.Fs = Fs

    def gen_filt_bank(self):
        self.fbank.gen(self.fullSigLen, 0, self.Fs)

    def applyFilters(self, sigIm, fAxis, melFloor=1e-6):
    
        sigParams = np.zeros(len(self.fbank))
        paramSetInd = 0

        for filt in self.fbank:

            filt.set_faxis(fAxis)
            filtSigIm = filt * sigIm
        
            sigParams[paramSetInd] = sum(filtSigIm)
            if sum(filtSigIm) < melFloor:
                sigParams[paramSetInd] = melFloor
            paramSetInd += 1

        return sigParams
    

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    print "Hello, World!"
