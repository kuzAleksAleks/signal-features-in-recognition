# To change this template, choose Tools | Templates
# and open the template in the editor.

import sys, os

from struct import unpack

from numpy import *

class CParamCollector:
    """
    Extracts features from parameter file and stores it in matrix
    """
    def __init__(self, paramFileName):
        self.paramFileName = paramFileName
        self.header = {} 
        self.sampleMatrix = []
        
    def htk_param_header(self, fileObj):
        """Read and returns header of the htk parameter file (e.g. *.mfc)"""
        
        block = fileObj.read(4)
        self.header['nSamples'] = unpack('>l', block )[0]
        block = fileObj.read(4)
        self.header['nPeriod'] = unpack('>l', block )[0]
        block = fileObj.read(2)
        self.header['sampSize'] = unpack('>h', block)[0]
        block = fileObj.read(2)
        self.header['sampKind'] = unpack('>h', block )[0]

    def next_sample_from_file(self, fileObject, sampleSize):
        """Reads from htk parameter file and return one sample"""
        FLOAT_SIZE = 4

        sample = []
        for smplCompIdx in range(sampleSize):
            compData = fileObject.read(FLOAT_SIZE)
            sample.append(unpack('>f', compData)[0])    # [0] becouse it returns tuple

        return sample

    def store_sample_matrix(self):
        """Reads from htk parameter file samples. Which are used to form the ample 
        matrix."""

        fileObj = open(self.paramFileName, 'rb')
        self.htk_param_header(fileObj)

        FLOAT_SIZE = 4

        for smplInd in range(self.header['nSamples']):
            sample = self.next_sample_from_file(fileObj, 
                                                self.header['sampSize'] // FLOAT_SIZE)
            self.sampleMatrix.append(sample)

    #    print sample
    #    plot_vector(sample)
    #    plot_matrix(array(sampleMatrix))

        fileObj.close()

class ParamFileReader(object):
    def __init__(self):
        pass
    
    def model_samples(self, model, paramfn, labfn):
        pass
    
    
class HTKParamFileReader(ParamFileReader):
    def _labs_from_file(self, labfpath):
        labs = []
        f = open(labfpath, 'r')
        for lab in f:
            labs.append(lab.split())
        f.close()
        
        return labs
        
        
    def model_samples(self, model, paramfn, labdb):
        fn = (os.path.basename(paramfn)).split('.')[0]
        corrLabfn = os.path.join(labdb, '.'.join([fn, 'lab']))
        #print corrLabfn
        paramCollector = CParamCollector(paramfn)
        paramCollector.store_sample_matrix()
        
        samples = []
        labs = self._labs_from_file(corrLabfn)
        for lBord, rBord, currModel in labs:
            lBord = int(int(lBord) / paramCollector.header['nPeriod'])
            rBord = int(int(rBord) / paramCollector.header['nPeriod'])
            if currModel == model:
                samples.extend(paramCollector.sampleMatrix[:][lBord:rBord])
        
        
        return samples
    
    
class SampleCollector(object):
    """
    Gathers samples from parameters files for particular model
    """
    def __init__(self, paramFileReader, modelName=None):
        self.paramFileReader = paramFileReader
        self.modelName = modelName
        self.corpus = []
           
    def store_corpus(self, model, paramDBPath, labDBPath, paramKind='mfc'):
        #if self.modelName == None:
        #    sys.stderr.write("Error! Model to be used is not defined!")
        #    return
        paramFNames = [fname for fname in os.listdir(paramDBPath) 
                      if fname.endswith(paramKind)]
        self.corpus = []
        for paramfn in paramFNames:
            paramfnPath = os.path.join(paramDBPath, paramfn)
            currSamples = self.paramFileReader.model_samples(model, paramfnPath, labDBPath)
            self.corpus.extend(currSamples)
    #        break
        #print len(self.corpus), len(self.corpus[0])
        
        
def main():
    args = sys.argv[1:]
    
    promptMessage = "--store paramDB labDB" 
    if not args:
        print promptMessage
        return
    
    if args[0] == '--store':
        if len(args) == 3:
            pfr = HTKParamFileReader()
            sc = SampleCollector(pfr)
            sc.store_corpus('a', args[1], args[2])
        else:
            print promptMessage
    else:
        print promptMessage
        
    
if __name__ == '__main__':
    main()