import os.path
#!/usr/bin/python -tt

import sys
import os
import yaml

from abc import ABCMeta, abstractmethod

import gen

class RecSysMaker:
    def __init__(self):
        self.lmm = LangModelMaker()
        
        self.recSysPath = self.lmm.recSysPath

        gen.gen_monophones(self.recSysPath, 'dict')
        gen.gen_trainlist(self.recSysPath)

        monophFile = open(os.path.join(self.recSysPath, 'monophones'))
        monophones = (monophFile.read()).split()
        monophFile.close()

        self.HInit = HInitShell(monophones)
        self.HRest = HRestShell(monophones)
        self.HERest = HERestShell()
        self.HHEd = HHEdShell()

    def create(self):
        try:
            #self.lmm.execute()
            self.HInit.execute()
            self.HRest.execute()
            self.HHEd.execute()
            return

            for iterInd in range(3):
                self.HERest.execute()

            self.HHEd.execute()

            for iterInd in range(5):
                self.HERest.execute()

        except IOError:
            sys.stderr.write("Error! Essensial file is absent!")
            sys.exit(1)

class Shell:
    __metaclass__ = ABCMeta

    recSysPath = 'recsystem'

    dirName = 'hmm'
    dirCount = 0

    stream = file('HTKCommands.yaml', 'r')
    HTKCommands = yaml.load(stream.read())
    stream.close()
    
    def __init__(self):
        if not os.path.exists(self.recSysPath):
            sys.stderr.write('Error! Can not find the appropriate information!')
            sys.exit(1)
    def inc_dir_count(self):
        Shell.dirCount += 1
    @abstractmethod
    def execute(self):
        pass

class LangModelMaker(Shell):
    def __init__(self, netFile='gram'):
        Shell.__init__(self)
        self.name = 'HParse'
        self.netFile = os.path.join(self.recSysPath, netFile)
        self.latFile = os.path.join(self.recSysPath, 'wdnet')
    def execute(self):
        cmd = ' '.join([self.HTKCommands['HParse'], self.netFile, self.latFile])
        os.system(cmd)
        if not os.path.basename(self.latFile) in os.listdir(self.recSysPath):
            sys.stderr.write('Error! {} was not created!'.format(self.latFile))
            sys.exit(1)
        print 'Step 1: language model has been created.'
    
class HInitShell(Shell):
    def __init__(self, monophones = []):
        self.name = 'HInit'
        self.cmdPatt = self.HTKCommands[self.name]
        self.monophones = monophones

    def execute(self):
        currHMMdir = self.dirName + str(self.dirCount)
        currHMMpath = os.path.join(self.recSysPath, currHMMdir)
        protofn = 'proto'

        if currHMMdir not in os.listdir(self.recSysPath):
            os.system(' '.join(['mkdir', currHMMpath]))

        for monoph in self.monophones:
            cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''), \
                             hmmdir=currHMMdir,
                             phoneme=monoph)
            os.system(cmd)
            ifname = os.path.join(currHMMpath, protofn)
            ofname = os.path.join(currHMMpath, '"' + monoph + '"')
            os.system(' '.join(['mv', ifname, ofname]))

        for monoph in os.listdir(currHMMpath):
            gen.change_acoustic_name(currHMMpath, monoph, protofn)

        self.inc_dir_count()
        
        print 'Step 2 (HInit): acoustic models first approach has been created.'

class HRestShell(Shell):
    def __init__(self, monophones = []):
        self.name = 'HRest'
        self.monophones = monophones
        self.cmdPatt = self.HTKCommands[self.name]
    
    def execute(self):
        prevHMMdir = self.dirName + str(self.dirCount - 1)
        currHMMdir = self.dirName + str(self.dirCount)
        currHMMpath = os.path.join(self.recSysPath, currHMMdir)

        if currHMMdir not in os.listdir(self.recSysPath):
            os.system(' '.join(['mkdir', currHMMpath]))

        for monoph in self.monophones:
            cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''), \
                             hmmdirpr=prevHMMdir,
                             hmmdircur=currHMMdir,
                             phoneme=monoph)
            os.system(cmd)

        gen.gen_hmmdefs(currHMMpath)
        self.inc_dir_count()
        print 'Step 3 (HRest): improved single phoneme acoustic models had been created.'

class HERestShell(Shell):
    def __init__(self, monophones = []):
        self.name = 'HERest'
        self.monophones = monophones
        self.cmdPatt = self.HTKCommands[self.name]

    def execute(self):
        prevHMMdir = self.dirName + str(self.dirCount - 1)
        currHMMdir = self.dirName + str(self.dirCount)
        currHMMpath = os.path.join(self.recSysPath, currHMMdir)
        
        gen.gen_phones(os.path.join(self.recSysPath, 'phones'), False)

        if currHMMdir not in os.listdir(self.recSysPath):
            os.system(' '.join(['mkdir', currHMMpath]))

        cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''), 
                             hmmdirpr=prevHMMdir,
                             hmmdircur=currHMMdir)
        os.system(cmd)
        self.inc_dir_count()
    
class HHEdShell(Shell):
    def __init__(self, monophones = []):
        self.name = 'HHEd'
        self.monophones = monophones
        self.cmdPatt = self.HTKCommands[self.name]
    def execute(self):
        prevHMMdir = self.dirName + str(self.dirCount - 1)
        currHMMdir = self.dirName + str(self.dirCount)
        currHMMpath = os.path.join(self.recSysPath, currHMMdir)

        if currHMMdir not in os.listdir(self.recSysPath):
            os.system(' '.join(['mkdir', currHMMpath]))

        cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''),
                             hmmdirpr=prevHMMdir,
                             hmmdircur=currHMMdir)
        os.system(cmd)
        self.inc_dir_count()
        
# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  print "Hello, World!"
