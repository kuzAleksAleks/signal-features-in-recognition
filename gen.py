#!/usr/bin/python -tt

import os.path
SMPL_TOTAL = (60,)
TEST_SMPL_TOTAL = (51,)

import sys
import os
import yaml
import re

import numpy as np

# Define a main() function that prints a little greeting.
def change_acoustic_name(path, phoneme, protofn):
  phonemePath = os.path.join(path, phoneme)

  f = open(phonemePath, 'r')
  modelStr = re.sub(protofn, phoneme, f.read())
  f.close
  
  f = open(phonemePath, 'w')
  f.write(modelStr)
  f.close()

def gen_codetr(workDir):
  baseName = os.path.basename(workDir)
  dirName = os.path.dirname(workDir)
  if baseName == 'test':
    wavesPath = os.path.join(dirName, "testwaves")
    trainPath = os.path.join(dirName, "test")
    outFileName = os.path.join(dirName, 'codetest.scp')

  elif baseName == 'train':
    print 'train'
    wavesPath = os.path.join(dirName, "waves")
    trainPath = os.path.join(dirName, "train")
    outFileName = os.path.join(dirName, 'codetr.scp')

  f = open(outFileName, 'w')
  featureExt = '.mfc'
  trgtFiles = [filename for filename in os.listdir(wavesPath) if not filename.endswith('.lab')]
  for filename in trgtFiles:
    wpBaseName = os.path.basename(wavesPath)
    currLine = os.path.join(wpBaseName, filename) + '   '
    fileFeatName = filename + featureExt
    tpBaseName = os.path.basename(trainPath)
    currLine = currLine + os.path.join(tpBaseName, fileFeatName)
    f.write(currLine + '\n')
  f.close()

def gen_trainlist(path):
  trainPath = os.path.join(path, "train")
  ofile = os.path.join(path, 'trainlist')
  f = open(ofile, 'w')
  filelist = [filename for filename in os.listdir(trainPath) if not (filename.endswith('lab') or filename.endswith('lab~'))]
  for filename in filelist:
    currLine = os.path.join(trainPath, filename)
    f.write(currLine + '\n')
  f.close()

def gen_monophones(path, sourceFileName):
  sourceFileName = os.path.join(path, sourceFileName)
  f = open(sourceFileName, 'r')
  fileStr = f.read()
  f.close()
  lexems = fileStr.split()
  result = []
  for lex in lexems:
    if lex.islower():
      result.append(lex)
  result = sorted(result)
  def del_adj(s):
    res = []
    for item in s:
      if res == [] or item != res[-1]:
        res.append(item)
    return res
  result = del_adj(result)
  ofile = os.path.join(path, 'monophones')
  f = open(ofile, 'w')
  for lex in result:
    f.write(lex + '\n')
  f.close()
#    print lex,
  
def gen_phones(phonesfile, unlab):
  f = open(phonesfile + '.mlf', 'w')
  f.write('#!MLF!#\n')
  labPath = os.path.join(os.path.dirname(phonesfile), 'waves')
  labFilesList = [filename for filename in os.listdir(labPath) \
                  if filename.endswith('.lab')]
  for labfile in labFilesList:
    path = os.path.join(labPath, labfile)
    f.write('\"' + path + '\"' + '\n')
    currLabFile = open(path, 'r')
    for line in currLabFile:
      if unlab:
        f.write(line.split()[-1])
        f.write('\n')
      else:
        f.write(line)

    currLabFile.close()
    f.write('.\n')
  f.close()

def gen_hmmdefs(workFolder):
  phonesList = [ph for ph in os.listdir(workFolder) if \
                not ph.endswith('~') and not ph == 'hmmdefs']
  print phonesList
  f = open(os.path.join(workFolder, 'hmmdefs'), 'w')

  firstPhon = phonesList[0]
  currHMMfile = open(os.path.join(workFolder, firstPhon), 'r')
  currHHM = currHMMfile.read()
  currHMMfile.close()
  f.write(currHHM)

  for ph in phonesList[1:]:
    currHMMfile = open(os.path.join(workFolder, ph), 'r')
    currHHM = currHMMfile.read()
    currHMMfile.close()
    pos = currHHM.find('~h')
    f.write(currHHM[pos:])
  f.close()

def gen_scp(workDir):

  baseName = os.path.basename(workDir)
  dirName = os.path.dirname(workDir)
  if baseName == 'waves':
    outFileName = os.path.join(dirName, 'train.scp')
  elif baseName == 'test':
    outFileName = os.path.join(dirName, 'test.scp')
  else:
    print 'Error! diractory', workDir, ' does not math weather waves or testwaves!'
    sys.exit(1)
  
  f = open(outFileName, 'w')
  trgtFiles = [filename for filename in os.listdir(workDir) if not filename.endswith('.lab')]
  for filename in trgtFiles:
    currLine = os.path.join(workDir, filename)
    f.write(currLine + '\n')
  f.close()

def gen_mlf(path):
  tesrefPath = os.path.join(path, 'testwaves')
  typicalfn = os.listdir(tesrefPath)[0]
  fnTemplate = typicalfn.split('_')[0] + '_'
  
  f = open(os.path.join(path, 'testprompts'), 'r')
  fw = open(os.path.join(path, 'testref.mlf'), 'w')
  fw.write('#!MLF!#\n')

  for ind, line in enumerate(f):
    pathLine = '\"' + os.path.join(tesrefPath, fnTemplate + str(ind + 1) + \
                '.lab') + '\"'
    fw.write(pathLine + '\n')
    words = line.split()
    fw.write('\n'.join(words + ['.']) + '\n')
  f.close()
  fw.close()
  
def gen_tree(command, threshold, monophones):
  fmono = open(monophones, 'r')
  phonemes = fmono.read()

  ftree = open('tree.hed', 'w')

  ftree.write('RO 100.0 stats\n\nTR 0\n')
  
  notInclPhonemes = ['sil', 'sp', '']
  phonemes = [phoneme for phoneme in phonemes.split('\n') if phoneme not in notInclPhonemes]

  stream = file('phoneme_classes_by.yaml', 'r')
  phClassesDict = yaml.load(stream)
  stream.close()

  for phClass in phClassesDict.keys():
    lineR = ''
    lineL = ''
    for phoneme in phonemes:
      if phoneme in phClassesDict[phClass]:
        if lineR or lineL:
          lineR += ', *+' + phoneme
          lineL += ', ' + phoneme + '-*'
        else:
          lineR += 'QS "R_' + phClass + '" { *+' + phoneme
          lineL += 'QS "L_' + phClass + '" {' + phoneme + '-*'
    
    if lineR or lineL:
      ftree.write(lineR + ' }\n')
      ftree.write(lineL + ' }\n')

  for phoneme in phonemes:
    line = 'QS "L_' + phoneme + '" {' + phoneme +'-*}\n'
    ftree.write(line)

  for phoneme in phonemes:
    line = 'QS "R_' + phoneme + '" {*+' + phoneme +'}\n'
    ftree.write(line)

  ftree.write('\n')
  ftree.write('TR 2\n')

  for stateInd in range(2, 5):
    for phoneme in phonemes:
      line = command + ' ' + str(threshold) + ' "' + phoneme + \
		'_s' + str(stateInd) + '" ' + '{(' + phoneme + \
		', *-' + phoneme + ', *-' + phoneme + '+*, ' + \
		phoneme + '+*).state[' + str(stateInd) + ']}\n'

      ftree.write(line)
  
  ftree.write('\nTR 1\nAU "triphones1"\nCO "tiedlist"\nST "trees"\n')

  fmono.close()
  ftree.close()
  
def gen_proto(dim, statesTotal=5, covType='VARIANCE', mixesTotal=1):
  initVal = 0.0
  initValDiag = 1.0
  meanVectTxt = ' '.join([str(initVal)] * dim) + '\n'
  covMatr = ''
  
  if covType == 'INVCOVAR':
    for rowInd in range(dim):
      covMatr += ' ' * (len(str(initVal)) + 1) * rowInd
      row = [str(initValDiag)]
      row.extend([str(initVal)] * (dim - rowInd - 1))
      covMatr += ' '.join(row) + '\n'
  elif covType == 'VARIANCE':
    covMatr += ' '.join([str(initValDiag)] * dim) + '\n'
  else:
    print 'Error! Invalid covarience type'
    sys.exit(1)

  f = open('proto', 'w')
  f.write(' '.join(['~o', '<VecSize>', str(dim), '<USER>', '\n']))
  f.write('~h "proto"' + '\n')
  f.write('<BeginHMM>' + '\n')

  f.write(' '.join(['<NumStates>', str(statesTotal), '\n']))

  for stInd in range(2, statesTotal):
    f.write('<STATE>' + ' ' + str(stInd) + '\n')
    if mixesTotal > 1:
      f.write('<NUMMIXES>' + ' ' + str(mixesTotal) + '\n')
      for mInd in range(1, mixesTotal + 1):
        f.write(' '.join(['<MIXTURE>', str(mInd), str(1.0 /  mixesTotal), '\n']))
        f.write('\n'.join([' '.join(['<MEAN>', str(dim)]), meanVectTxt]))
        f.write('\n'.join([' '.join(['<' + covType + '>', str(dim)]), covMatr]))
    else:
      f.write('\n'.join([' '.join(['<MEAN>', str(dim)]), meanVectTxt]))
      f.write('\n'.join([' '.join(['<' + covType + '>', str(dim)]), covMatr]))

  f.write(' '.join(['<TRANSP>',  str(statesTotal), '\n']))

  trMatr = np.zeros([statesTotal, statesTotal])
  trMatr[0][1] = 1.0
  for diagElInd in range(1, statesTotal - 2):
    trMatr[diagElInd][diagElInd] = 0.6
    trMatr[diagElInd][diagElInd + 1] = 0.4
  trMatr[statesTotal - 2][statesTotal - 2] = 0.7
  trMatr[statesTotal - 2][statesTotal - 1] = 0.3

  for row in trMatr:
    f.write(' '.join(map(str, row)) + '\n')

  f.write('<EndHMM>')
  f.close()


def hint(validArgs):
  print 'Please, choose somesthing from the list:'
  for arg in validArgs:
    print arg

def main():
  validArgs = ['--codetr', '--trainlist', '--monophones', '--phones [unlab]' , 
               '--hmmdefs', '--train', '--scp', '--testref', '--tree', 
               '--mlf', '--proto']
  outMessage = 'Done!'
#  print sys.argv[1]
  args = sys.argv[1:] 

  if not args:
    print hint(validArgs)
    sys.exit(1) 
    
  if len(args) >= 1:
    if args[0] == '--codetr':
      if len(args) == 2:
        gen_codetr(args[1])
      else:
        print 'usage: --codetr path/train | path/test'
        sys.exit(1)
      

    elif args[0] == '--trainlist':
      if len(args) == 2:
        gen_trainlist(args[1])
      else:
        print 'USAGE: --trainlist pathToWave'
        sys.exit(1)

    elif args[0] == '--monophones':
      if len(args) == 2:
        path = args[1]
        sourceFileName = 'dict'
      elif len(args) == 3:
        path = args[1]
        sourceFileName = args[2]
      else:
        print 'USAGE: dictfilepath [dictfilename]'
        sys.exit(1) 
      gen_monophones(path, sourceFileName)

    elif args[0] == '--phones':
      if len(args) == 1:
        gen_phones('phones', False)
      elif len(args) == 2 and args[1] == 'unlab':
        gen_phones('phones', True)
      else:
        print 'usage: phones [unlab]'

    elif args[0] == '--hmmdefs':
      if len(args) == 2:
        gen_hmmdefs(args[1])
      else:
        print 'usage: --hmmdefs foldwithph'
        sys.exit(1)

    elif args[0] == '--scp':
      if len(args) == 2:
        gen_scp(args[1])
      else:
        print 'usage: --scp waves | test'
        sys.exit(1)

    elif sys.argv[1] == '--mlf':
      if len(args) == 2:
        gen_mlf(args[1])
      else:
        print 'usage: --mlf recsyspath'
        
    elif args[0] == '--tree':
      if len(args) == 4:
      	gen_tree(args[1], args[2], args[3])
      else:
        print 'usage --tree command threshold monophones'
        sys.exit(1)

    elif args[0] == '--proto':
      if len(args) == 5:
        gen_proto(int(args[1]), int(args[2]), args[3], int(args[4]))
      elif len(args) == 4:
        gen_proto(int(args[1]), int(args[2]), args[3])
      elif len(args) == 3:
        gen_proto(int(args[1]), int(args[2]))
      elif len(args) == 2:
        gen_proto(int(args[1]))
      else:
        print 'usage --proto dim statesTotal covType(VARIANCE | INVCOVAR) mixesTotal'
        sys.exit(1)
    
    else:
      print hint(validArgs)
      outMessage = 'Is not ' + outMessage

  else:
    outMessage = 'Generate what ever you want!'
  print outMessage 

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()
