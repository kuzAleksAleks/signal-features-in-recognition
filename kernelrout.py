#! /usr/bin/python

import os, pickle, yaml
import numpy as np

from param_file_routines import HTKParamFileReader, SampleCollector

def samples_corpus(paramDBPath, labDBPath):
    pfr = HTKParamFileReader()
    
    recSysDir = 'recsystem'
    monophPath = os.path.join(recSysDir, 'monophones')
    f = open(monophPath, 'r')
    monophones = f.read().split()
    f.close()
    
    #monophones = ['a', 'o', 'k']
    print monophones
     
    samples = {}
    sc = SampleCollector(pfr)
    for ph in monophones:
        sc.store_corpus(ph, paramDBPath, labDBPath)
        samples[ph] = sc.corpus[:]
    
    return samples


def all_samples(paramDBPath, labDBPath):
    samples = samples_corpus(paramDBPath, labDBPath)
    train = []
    target = []
    for ph in samples.keys():
        train += samples[ph]
        target += [ph] * len(samples[ph])
        
    return train, target
