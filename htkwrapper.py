#! /usr/bin/python

from struct import pack
import numpy as np

class HTKParamFormat(object):
    def __init__(self, configfn):
        self.config = self._config_from_file(configfn)
        
    def __str__(self):
        return str(self.config) 
    
    def _config_from_file(self, confPath):
        
        f = open(confPath, 'r')

        config = {}
        for line in f:
            if not line.startswith('#'):
                key, val = line[:-1].split(' = ')
                config[key] = ''.join(val.split())

                if val.isdigit():
                    config[key] = int(val)
                elif (val.split('.')[0]).isdigit():
                    config[key] = float(config[key])

                if ''.join(val.split()) == 'T':
                    config[key] = True
                elif ''.join(val.split()) == 'F':
                    config[key] = False

        f.close()

        config['Fs'] = 16000
        return config
    
    
    def _HTK_header(self, nSamples, sampPeriod, sampSize, parmKind):
        header = ''

        header += pack('>I', nSamples)
        header += pack('>I', sampPeriod)

        sampSize = np.int16(sampSize)
        header += pack('>H', sampSize)

        parmKind = np.int16(parmKind)
        header += pack('>H', parmKind)

        return header  
    
    def htk_format(self, paramVect, parmKind=9):
        transDataStr = ''

        for sampleInd in range(len(paramVect)):
            sample = ''
            for val in paramVect[sampleInd]:
                sample += pack('>f', val)
            transDataStr += sample

        #sampSize = len(self.transformer.transform(sig, 0, smplsInWind))
        nSamples = len(paramVect)
        sampSize = len(sample)
        
        transSigStr = self._HTK_header(nSamples, self.config['TARGETRATE'], 
                                       sampSize, parmKind) + transDataStr
        return transSigStr
    
    