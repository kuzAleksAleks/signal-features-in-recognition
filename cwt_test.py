import sys

import numpy as np
from param_gen import sig_from_file, SigFilter, Differentiator, FilterBank
import matplotlib.pyplot as plt
import mlpy.wavelet as wave
import mlpy
from scipy.fftpack import fft
from scipy.signal import triang, hamming
import scipy.signal

def cwt_test(fname, dt=(1.0 / 16000), omega0=8):
    sig = np.array(sig_from_file(fname))
    
    scales = wave.autoscales(N=len(sig), dt=dt, dj=0.25, wf='morlet', p=omega0) 
    
    S = wave.cwt(x=sig, dt=dt, scales=scales, wf='morlet', p=omega0) 
    
    print S.shape

def cwt(sig, dt=(1.0 / 16000), wf='morlet', p=8):
    scales = wave.autoscales(N=len(sig), dt=dt, dj=0.25, wf=wf, p=p)
     
    wavIm = wave.cwt(x=sig, dt=dt, scales=scales, wf=wf, p=p)

    return wavIm, scales

def wind_fft(sig, leftBord, rightBord, dt):
    T = (rightBord - leftBord) * dt
    df = 1.0 / T
    f = np.arange(0, df * ((rightBord - leftBord) // 2), df)

    fourIm = fft(sig[leftBord:rightBord])

    return fourIm, f

def cwt_vs_fft(fname, leftBord, rightBord, dt=(1.0 / 16000), wf='morlet', omega0=8):
    sig = np.array(sig_from_file(fname))
    
    wavIm, scales = cwt(sig, dt, wf, omega0)

    fperiods = wave.fourier_from_scales(scales=scales, wf=wf, p=omega0)
    frqs = 1 / fperiods
 
    fourIm, f = wind_fft(sig, leftBord, rightBord, dt)
    
    fig = plt.figure(1)
    ax1 = plt.subplot(2, 1, 1)
    print len(f), (rightBord - leftBord) // 2
    p1 = ax1.plot(f, np.abs(fourIm[:(rightBord - leftBord) // 2]))
    ax2 = plt.subplot(2, 1, 2)
    p2 = ax2.plot(frqs, np.abs(wavIm[:, (leftBord + rightBord) // 2]))
    
    plt.show()    
    

def frq2mel(f):
    return 1127.0 * np.log(1.0 + f / 700.0)

def mel2frq(m):
    return  (np.exp(m / 1127.0) - 1.0) * 700.0

def gen_filterbank(fname, ftype):
    sig = np.array(sig_from_file(fname))

    windKind = scipy.signal.__dict__[ftype]
    
    fs = 16000
    dt = 1.0 / fs
    T = len(sig) * dt
    df = 1.0 / T
    f = np.arange(0, fs / 2, df)
    
    chTotal = int(np.ceil(4.6 * np.log10(fs)))

    mlo = frq2mel(0.0)
    mhi = frq2mel(fs/2)
    mb = mhi - mlo

    mc = np.zeros(chTotal)
    for chInd in range(chTotal):
        mc[chInd] = ((chInd + 1) / np.float(chTotal)) * mb + mlo

    fc = map(mel2frq, mc)

    filtBank = []
    lInd = 0
    rInd = int(fc[1] / df)
    for chInd in range(2, chTotal + 1):
        N = rInd - lInd
        trw = windKind(N)

        F = np.zeros(len(f))
        F[lInd:rInd] = trw

        filtBank.append(F)

        if (chInd > 19):
            break

        lInd = int(fc[chInd - 2] / df)
        rInd = int(fc[chInd] / df)

    return filtBank, f

def filtering(fname):
    sig = np.array(sig_from_file(fname))

    fs = 16000
    dt = 1.0 / fs
    T = len(sig) * dt
    df = 1.0 / T

    wf = 'morlet'
    omega0 = 8
    
    wavIm, scales = cwt(sig, dt, wf, omega0)
    fperiods = wave.fourier_from_scales(scales=scales, wf=wf, p=omega0)
    frqs = 1 / fperiods
     
    filtBank, f = gen_filterbank(fname, 'hamming')
    
    wavSliceInd = 13500
    
    filtInd = 12
    filt = filtBank[filtInd]

    filtObj = SigFilter(filt, len(sig), fs)
    filtObj.set_faxis(frqs)
    filtWavIm = filtObj * wavIm[:, wavSliceInd]

    fig = plt.figure(1)
    ax1 = plt.subplot(2, 1, 1)
    p1 = ax1.plot(f, filtObj[:])
    plt.grid(True)
    ax2 = plt.subplot(2, 1, 2)
    p2 = ax2.plot(frqs, np.abs(wavIm[:, wavSliceInd]))
    p2 = ax2.plot(frqs, filtWavIm)
    plt.grid(True)
    plt.show()    

def diff(data, ind, win):
    denom = win * (win + 1) * (2 * win + 1) / 6.0
    denom *= 2
    num = np.zeros(data.shape[0], dtype=data.dtype)
    leftInd, rightInd = ind, ind
    for i in range(1, win + 1):
        if leftInd - 1 >= 0:
            leftInd -= 1
        if rightInd < data.shape[1] - 1:
            rightInd += 1
        num += i * (data[:, rightInd] - data[:, leftInd])
    return num / denom

def differentiate(fname, win):
    sig = sig_from_file('recsystem/waves/S_0')
   
    wavIm, scales = cwt(sig, 1.0 / 16000, 'morlet', 8)
    print wavIm[:, 0]
    print 
    differ = Differentiator(win)
    #diffs = np.array([diff(wavIm, i, win) for i in range(wavIm.shape[1])])
    #diffs = diffs.transpose()

    diffs = differ.diff(wavIm)
    print diffs[:, 0]
    print

    wavImDiffs = np.vstack([wavIm, diffs])

    return wavImDiffs
   
def fbankappl(fname):
    
    sig = np.array(sig_from_file(fname))

    fs = 16000
    dt = 1.0 / fs
    T = len(sig) * dt
    df = 1.0 / T

    wf = 'morlet'
    omega0 = 8
    
    wavIm, scales = cwt(sig, dt, wf, omega0)
    fperiods = wave.fourier_from_scales(scales=scales, wf=wf, p=omega0)
    frqs = 1 / fperiods
    
    wavSliceInd = 48775 # 'a' in recsysem/waves/S_58
        
    transSig = np.abs(wavIm[:, wavSliceInd])

    windType = 'hamming' 

    fbank = FilterBank(windType)
    fbank.gen(len(sig))
    
    fig = plt.figure(1)
    ax1 = plt.subplot(2, 1, 1)
    plt.grid(True)
    ax2 = plt.subplot(2, 1, 2)
    p2 = ax2.plot(frqs, transSig)
    
    sigParams = np.zeros(len(fbank))
    paramSetInd = 0

    for filt in fbank:

        filt.set_faxis(frqs)
        filtWavIm = filt * transSig
        
        sigParams[paramSetInd] = sum(np.abs(filtWavIm))
        paramSetInd += 1
        
        p1 = ax1.plot(fbank.fAxis, filt[:])
        p2 = ax2.plot(frqs, np.abs(filtWavIm))

    plt.grid(True)
    plt.show()
    
    p = plt.plot(sigParams)
    plt.grid(True)
    plt.show()

def fbankappl_fft(fname):
    
    sig = np.array(sig_from_file(fname))

    Fs = 16000
    dt = 1.0 / Fs
    
    leftBord = 48575
    rightBord = 48975
    
    sigFrameLen = rightBord - leftBord
    T = sigFrameLen * dt
    df = 1.0 / T

    frqs = np.arange(0, Fs / 2, df)
    dfFilt = 1.0 / (len(sig) * dt)
    fAxis = np.arange(0, Fs / 2, dfFilt)

    S = fft(sig[leftBord:rightBord])
    S = S[:len(S) // 2]
    windType = 'hamming' 

    fbank = FilterBank(windType)
    fbank.gen(len(sig))
    
    wavSliceInd = 13500
    
    fig = plt.figure(1)
    ax1 = plt.subplot(2, 1, 1)
    plt.grid(True)
    ax2 = plt.subplot(2, 1, 2)
    p2 = ax2.plot(frqs, np.abs(S))
    
    sigParams = np.zeros(len(fbank))
    paramSetInd = 0

    for filt in fbank:

        filt.set_faxis(frqs)
        filtS = filt * S
        
        sigParams[paramSetInd] = sum(np.abs(filtS))
        paramSetInd += 1
        
        p1 = ax1.plot(fbank.fAxis, filt[:])
        p2 = ax2.plot(frqs, np.abs(filtS))

    plt.grid(True)
    plt.show()
    
    p = plt.plot(sigParams)
    plt.grid(True)
    plt.show()
    
def main():

    args = sys.argv[1:]
	
    if not args:
        print ['cwt_test', 'cwt_vs_fft', 'filter_bank',
               'filtering', 'fbankappl', 'differentiate', 'fbankappl_fft']
        sys.exit()

    if args[0] == '--cwt_test':
    	if len(args) != 2:
            print 'USAGE: cwt_test path_to_file'
        else:
            cwt_test(args[1])
    elif args[0] == '--cwt_vs_fft':
        if len(args) == 4:
            cwt_vs_fft(args[1], int(args[2]), int(args[3]))
        else:
            print 'USAGE: cwt_vs_fft path_to_file leftBord rightBord'

    elif args[0] == '--filter_bank':
        if len(args) == 3: 
#            filtBank, f = gen_filterbank(args[1], args[2])

            sig = sig_from_file(args[1])
            fbank = FilterBank(args[2])
            fbank.gen(len(sig))

            for filt in fbank:
                p = plt.plot(fbank.fAxis, filt.mapedVals)

            plt.grid(True)
            plt.show()
        else:
            filtTypes = ['triang', 'hamming']
            print "USAGE: cwt_vs_fft sig_fname filt_type (", filtTypes, ")"
            
    elif args[0] == '--filtering':
    	if len(args) == 2:
            filtering(args[1])
        else:
            print 'USAGE: cwt_test --filtering path_to_file'
    elif args[0] == '--fbankappl':
    	if len(args) == 2:
            fbankappl(args[1])
        else:
            print 'USAGE: cwt_test --fbankappl path_to_file'
    elif args[0] == '--fbankappl_fft':
    	if len(args) == 2:
            fbankappl_fft(args[1])
        else:
            print 'USAGE: cwt_test --fbankappl_fft path_to_file'
    elif args[0] == '--differentiate':
    	if len(args) == 3:
            diffs = differentiate(args[1], int(args[2]))
            print diffs.shape
            print diffs[:, 0]
        else:
            print 'USAGE: cwt_test --differentiate path_to_file win'
    else:
        print 'Wrong command!'

if __name__ == "__main__":
    main()
