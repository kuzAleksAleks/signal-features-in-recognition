# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__="User"
__date__ ="$12.09.2012 18:59:03$"

import os

import gen
from speech_system_maker import Shell

class ModelAnalyser():
    def __init__(self):
        self.HVite = HViteShell()
        self.HResults = HResultShell()
    def analyse(self):
        self.HVite.execute()
        self.HResults.execute()

class HViteShell(Shell):
    def __init__(self):
        Shell.__init__(self)
        self.name = 'HVite'
        self.cmdPatt = self.HTKCommands[self.name]
    def execute(self):
        HMMdirs = sorted([hmmdirs for hmmdirs in os.listdir(self.recSysPath)
                         if hmmdirs.startswith(self.dirName)], key=len)
        maxlen = len(HMMdirs[-1])
        currHMMdir = sorted([hmmdir for hmmdir in HMMdirs
                             if len(hmmdir) == maxlen])[-1]
        print currHMMdir
        gen.gen_scp(os.path.join(self.recSysPath, 'test'))

        cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''),
                                  hmmdircur=currHMMdir)
        print cmd
        os.system(cmd)

class HResultShell(Shell):
    def __init__(self):
        Shell.__init__(self)
        self.name = 'HResults'
        self.cmdPatt = self.HTKCommands[self.name]
        gen.gen_mlf(self.recSysPath)
    def execute(self):
        cmd = self.cmdPatt.format(sysdir=os.path.join(self.recSysPath, ''))
        os.system(cmd)

if __name__ == "__main__":
    print "Hello World";
