#! /usr/bin/python

__author__="User"
__date__ ="$07.09.2012 20:35:03$"

import sys
sys.path.append("/home/kuzaleks/Projects/emacs/kernel_learning")

import os
import glob
import shutil
import numpy as np
import pylab, pickle, mlpy
import matplotlib.pyplot as plt

from struct import pack
from speech_system_maker import RecSysMaker
from model_analyser import ModelAnalyser
from param_gen import HTK_TIME_UNITS_NORM
from param_gen import Extractor, FFTTransformer, WavletTransform, \
                      sig_from_file, save_data, Differentiator
from scikits.talkbox.features import mfcc
from htkwrapper import HTKParamFormat
from kernelrout import all_samples
from kernelmethods import kPLS, kPCA, kOPLS, rbf_closure, KernelRbf, distance_prop, kCCAWrapper
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def main():
    validArgs = ['--extract', '--extractmfcc', '--makerecsys', '--analyse', '--clear', '--makekerntransobj', '--createpcaobj']
    #print sys.argv[1]
    args = sys.argv[1:]
    recSysPath = 'recsystem'
    if not args:
       print validArgs
       sys.exit(1)

    if args[0] == '--makerecsys':
        rsm = RecSysMaker()
        rsm.create()
    elif args[0] == '--analyse':
        ma = ModelAnalyser()
        ma.analyse()
    elif args[0] == '--extract':
        if len(args) == 2:
            extr = Extractor(os.path.join(recSysPath, 'config'), FFTTransformer)
		
            flist = open(args[1], 'r')

            for entry in flist:
                infile, outfile = [os.path.join(recSysPath, fname) \
                                   for fname in entry.split()]
                sig = sig_from_file(infile)
                data = extr.transform(sig)
                save_data(data, outfile)
                print infile, ' --> ', outfile, len(sig)
			
            flist.close()
        else:
            print 'USAGE: --extract codetr_fname'
            
    elif args[0] == '--extractmfcc':
        if len(args) == 2:
#            f = open('recsystem/KPCA.pkl', 'rb')
#            mva = pickle.load(f)
#            f.close()
            
            flist = open(args[1], 'r')
            
            for entry in flist:
                infile, outfile = [os.path.join(recSysPath, fname) \
                                       for fname in entry.split()]
                sig = np.array(sig_from_file(infile))
                
                pFormat = HTKParamFormat(os.path.join(recSysPath, 'config'))
                Fs = pFormat.config['Fs']
                winsize = pFormat.config['WINDOWSIZE'] / HTK_TIME_UNITS_NORM * Fs
                
                ceps, mspec, spec = mfcc(sig, nwin=winsize, fs=Fs)
                
 #               differ = Differentiator(2)
 #               diffs = differ.diff(ceps.T)
 #               diffsDiffs = differ.diff(diffs)
            
 #               paramVect = np.hstack([ceps, diffs.T, diffsDiffs.T])
                paramVect = ceps
                
                #plt.figure(1)
                #plt.subplot(211)
                #plot1 = plt.plot(paramVect[0])

#                paramVect = mva.transform(paramVect, k=len(paramVect[0]))
#                paramVect = mva.transform(paramVect, len(paramVect[0]))
                #plt.subplot(212)
                #plot1 = plt.plot(paramVect[0])
                #plt.show()
                
                #break
            
                parmKind = 9
                                               
                transSigStr = pFormat.htk_format(paramVect, parmKind)
                save_data(transSigStr, outfile)
                print infile, ' --> ', outfile, len(sig)
              
            flist.close()
        else:
            print 'USAGE: --extractmfcc codetr_fname'
        
    elif args[0] == '--makekerntransobj':
        if len(args) == 3:
            paramDBPath, labDBPath = args[1:]
            train, target = all_samples(paramDBPath, labDBPath)
            
      #      kernel_func = mlpy.KernelPolynomial(gamma=1.0, b=0.0, d=4.0)
            median = distance_prop(train, np.median)
            print "median:", median
#            kernel_func = mlpy.KernelGaussian(sigma=distance_prop(train, np.median))
            sigma = 20
            print "sigma:"
            kernel_func = KernelRbf(sigma)

#            kmva = mlpy.KPCA(kernel_func)
#            kmva.learn(train)
            #kpca = kPCA(kernel_func)
            kmva = kOPLS(kernel_func)
            print "dim(train) = ", type(train), len(train)
            #kpca.estim_kbasis(train)
            kmva.estim_kbasis(train, target, int(len(train) * 0.5))
            #kmva.estim_kbasis(train)
            print kmva.__class__.__name__ + " created"
            f = open(os.path.join('recsystem', kmva.__class__.__name__ + '.pkl'), 'wb')
            pickle.dump(kmva, f)
            f.close()
        else:
            print "USAGE: --makekernmatr paramDBPath labDBPath"

    elif args[0] == '--createpcaobj':
        if len(args) == 3:
            paramDBPath, labDBPath = args[1:]
            train, target = all_samples(paramDBPath, labDBPath)
            print len(train)
            
            pylab.plot(train[0])
            pylab.show()
            
            pca = mlpy.PCA()
            pca.learn(np.array(train))
            f = open('recsystem/pca.pkl', 'wb')
            pickle.dump(pca, f)
            f.close()
        else:
            print 'USAGE: --createpcaobj paramDBPath labDBPath'
    elif args[0] == '--createccaobj':
        if len(args) == 4:
            Kxfn, Wxfn, trainfn = args[1:]
            f = open(trainfn, 'r')
            data = []
            for line in f:
                if not line.startswith('#') and len(line.split()) > 0:
                    data.append([float(num) for num in line.split()])
            f.close()
            print len(data), distance_prop(data, np.median)
#            kernel_func = mlpy.KernelGaussian(sigma=distance_prop(data, np.median))
            kernel_func = KernelRbf(distance_prop(data, np.median))
            kcca = kCCAWrapper(kernel_func)
            kcca.load_basis(Kxfn, Wxfn, trainfn)
            print kcca.Kx.shape, kcca.vecs.shape, len(kcca.trData)
            print kcca.__class__.__name__ + " created"
            f = open(os.path.join('recsystem', kcca.__class__.__name__ + '.pkl'), 'wb')
            pickle.dump(kcca, f)
            f.close()

    elif args[0] == '--clear':
        try:
            os.remove(os.path.join(recSysPath, 'recout.mlf'))
            os.remove(os.path.join(recSysPath, 'test.scp'))
            os.remove(os.path.join(recSysPath, 'trainlist'))
            #os.remove(os.path.join(recSysPath, 'wdnet'))
        except OSError:
            pass
        
        fTrainList = glob.glob(os.path.join(recSysPath,'train/*.mfc'))
        for fTrain in fTrainList:
            os.remove(fTrain)

        fTestList = glob.glob(os.path.join(recSysPath,'test/*.mfc'))
        for fTest in fTestList:
            os.remove(fTest)
        
        hmmDirList = glob.glob(os.path.join(recSysPath,'hmm*'))
        for dhmm in hmmDirList:
            shutil.rmtree(dhmm)
    else:
        print "Invalid command."
        
if __name__ == "__main__":
    print "Hello World";
    main()
